﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class Box : Game_Object
    {
        public int Stage;
        public Box(Control Obj,int Stage) : base(Obj)
        {
            Movable = true;
            this.Stage = Stage;
        }
        public int GetStage()
        {
            return Stage;
        }
    }
}
