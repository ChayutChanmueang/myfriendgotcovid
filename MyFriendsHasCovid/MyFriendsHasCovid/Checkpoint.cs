﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class Checkpoint : Game_Object
    {
        int Stage;
        public Checkpoint(Control Obj, int Stage) : base(Obj)
        {
            this.Stage = Stage;
        }
        public int GetStage()
        {
            return Stage;
        }
    }
}
