﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class DropOff : Game_Object
    {
        int Stage;
        bool Occupied = false;
        public DropOff(Control Obj, int Stage) : base(Obj)
        {
            this.Stage = Stage;
        }
        public int GetStage()
        {
            return Stage;
        }
        public void SetOccupied(bool NewOc)
        {
            Occupied = NewOc;
        }
        public bool GetOccupied()
        {
            return Occupied;
        }
    }
}
