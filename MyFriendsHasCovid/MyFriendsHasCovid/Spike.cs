﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class Spike : Game_Object
    {
        bool Activated;
        private Bitmap[] Spike_Anim = new Bitmap[2];
        public Spike(Control Obj,bool Activated) : base(Obj)
        {
            this.Activated = Activated;
            LoadSprite();
            if (Activated == true)
            {
                Obj.BackgroundImage = Spike_Anim[1];
            }
            else if (Activated == false)
            {
                Obj.BackgroundImage = Spike_Anim[0];
            }
        }
        public bool GetActivated()
        {
            return Activated;
        }
        public void FlipFlopActivate()
        {
            if(Activated==true)
            {
                Activated = false;
                Obj.BackgroundImage = Spike_Anim[0];
            }
            else if (Activated == false)
            {
                Activated = true;
                Obj.BackgroundImage = Spike_Anim[1];
            }
        }
        private void LoadSprite()
        {
            Spike_Anim[0] = new Bitmap("./Sprites/Spike/Spikes_Off_48.png");
            Spike_Anim[1] = new Bitmap("./Sprites/Spike/Spikes_On_48.png");
        }
    }
}
