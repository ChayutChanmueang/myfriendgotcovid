﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    public partial class Form1 : Form
    {
        //Game Var
        bool Ironman_Mode = false;
        int ResetCount = 0;
        bool Debuging = false;
        bool GameEnd = false;
        int Room1_Count = 0;
        int Room2_Count = 0;
        int Room3_Count = 0;
        int Room1_Max = 0;
        int Room2_Max = 0;
        int Room3_Max = 0;
        int FlickerCount = 0;
        //Timer
        int MiliSec = 0;
        int Sec = 0;
        int Min = 0;
        //var that's used for everything
        int Move_Distance = 48;
        int CharMove_Speed = 12; // must be devidable by Move_Distance or it will bug (Devided by 2 is reccommended)
        int counter = 0;
        //Player's var
        bool P_GoLeft;
        bool P_GoRight;
        bool P_GoUp;
        bool P_GoDown;
        int P_SpawnX = 576;
        int P_SpawnY = 240;
        int P_EndPosX = 0; //P_EndPos are telling how far form the Spawn is also added to not make the movement fucked up
        int P_EndPosY = 0;
        int Life = 3;
        bool PlayerIsHit = false;
        bool Dead = false;
        //Player Anim
        private Bitmap[][] P_Anim = new Bitmap[12][];
        int State = 0; 
        // 0 = idle front 1 = idle left 2 = idle right 3 = idle back 
        // 4 = walk down 5 = walk left 6 = walk right 7 = walk up 
        // 8 = push down 9 = push left 10 = push right 11 = push up
        private int FrameNum = 0;
        int checkpoint = 1;
        //Game Object Var
        List<Box> Boxes = new List<Box>();
        string BoxNameList; //Debug Only
        List<Control> Walls = new List<Control>();
        string WallNameList; //Debug Only
        List<Checkpoint> Checkpoints = new List<Checkpoint>();
        List<DropOff> DropOffs = new List<DropOff>();
        List<CovBox> CovBoxes = new List<CovBox>();
        List<Covid> Covids = new List<Covid>();
        List<Spike> Spikes = new List<Spike>();

        public Form1()
        {
            InitializeComponent();
            UpdateLists();
            LoadSprite();
        }

        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            //Player Input
            if(e.KeyCode==Keys.R)
            {
                GameRestart();
            }
            if (e.KeyCode == Keys.Left)
            {
                P_GoLeft = true;
                P_GoUp = false;
                P_GoDown = false;
                P_GoRight = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                P_GoRight = true;
                P_GoUp = false;
                P_GoDown = false;
                P_GoLeft = false;
            }
            if(e.KeyCode==Keys.Up)
            {
                P_GoUp = true;
                P_GoDown = false;
                P_GoLeft = false;
                P_GoRight = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                P_GoDown = true;
                P_GoUp = false;
                P_GoLeft = false;
                P_GoRight = false;
            }
            if (e.KeyCode == Keys.I && checkpoint < 2)
            {
                if(Ironman_Mode == false)
                {
                    Ironman_Mode = true;
                    Life = 1;
                    P_EndPosX = 0;
                    P_EndPosY = 0;
                    P_SpawnX = Checkpoint1.Left;
                    P_SpawnY = Checkpoint1.Top;
                    Player.Left = Checkpoint1.Left;
                    Player.Top = Checkpoint1.Top;
                    FlickerCount = 0;
                    PlayerIsHit = false;
                    Player.Visible = true;
                    State = 0;
                    ResetBoxPos();
                    ResetDF();
                    ResetTimer();
                }
                else if (Ironman_Mode ==true)
                {
                    Ironman_Mode = false;
                    Life = 3;
                    P_EndPosX = 0;
                    P_EndPosY = 0;
                    P_SpawnX = Checkpoint1.Left;
                    P_SpawnY = Checkpoint1.Top;
                    Player.Left = Checkpoint1.Left;
                    Player.Top = Checkpoint1.Top;
                    FlickerCount = 0;
                    PlayerIsHit = false;
                    Player.Visible = true;
                    State = 0;
                    ResetBoxPos();
                    ResetDF();
                    ResetTimer();
                }
            }
            //Debuging Keys
            if (e.KeyCode == Keys.Z)
                Debuging = true;
            if (e.KeyCode == Keys.X)
                Debuging = false;
            if (e.KeyCode == Keys.NumPad1&&Debuging==true)
            {
                checkpoint = 1;
            }
            if (e.KeyCode == Keys.NumPad2 && Debuging == true)
            {
                checkpoint = 2;
            }
            if (e.KeyCode == Keys.NumPad3 && Debuging == true)
            {
                checkpoint = 3;
            }

        }
        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            //Player Input
            if(e.KeyCode==Keys.Left)
            {
                P_GoLeft = false;
                Txt_DebugPlayerDi.Text = "Player Go :"; //Debug
                State = 1;
            }
            else if (e.KeyCode == Keys.Right)
            {
                P_GoRight = false;
                Txt_DebugPlayerDi.Text = "Player Go :"; //Debug
                State = 2;
            }
            else if (e.KeyCode == Keys.Up)
            {
                P_GoUp = false;
                Txt_DebugPlayerDi.Text = "Player Go :"; //Debug
                State = 3;
            }
            else if (e.KeyCode == Keys.Down)
            {
                P_GoDown = false;
                Txt_DebugPlayerDi.Text = "Player Go :"; //Debug
                State = 0;
            }
        }

        private void MainGameTimerEvent(object sender, EventArgs e)
        {
            //DEBUG HUD
            if(Debuging == true)
            {
                Txt_PosDebug.Visible = true;
                Txt_PosDebug2.Visible = true;
                Txt_SizeDebug.Visible = true;
                Txt_BoxNameDebug.Visible = true;
                Txt_WallNameDebug.Visible = true;
                Txt_DebugPlayerDi.Visible = true;
                Txt_RoomCountDebug.Visible = true;
                Txt_PosDebug.Text = "Player.Left : " + Player.Left + " Player.Top : " + Player.Top;
                Txt_PosDebug2.Text = "P_EndPosX : " + (P_EndPosX + P_SpawnX) + " P_EndPosY : " + (P_EndPosY + P_SpawnY);
                Txt_SizeDebug.Text = "Player.X : " + Player.Size.Width + " Player.Y : " + Player.Size.Height;
                Txt_BoxNameDebug.Text = "Life : " + Life;
                Txt_WallNameDebug.Text = "Player is Hit : " + PlayerIsHit.ToString() +" Flicker Count : " +FlickerCount;
                Txt_RoomCountDebug.Text = "Frame : " + counter;

            }
            else
            {
                Txt_PosDebug.Visible = false;
                Txt_PosDebug2.Visible = false;
                Txt_SizeDebug.Visible = false;
                Txt_BoxNameDebug.Visible = false;
                Txt_WallNameDebug.Visible = false;
                Txt_DebugPlayerDi.Visible = false;
                Txt_RoomCountDebug.Visible = false;
            }
            if(Life==3)
            {
                Lifes.Width = 75;
            }
            else if(Life==2)
            {
                Lifes.Width = 50;
            }
            else if (Life == 1)
            {
                Lifes.Width = 25;
            }
            else if (Life == 0)
            {
                Lifes.Width = 0;
                Dead = true;
            }
            if (Dead == true)
            {
                GameTimer.Stop();
            }
            Txt_ResetCount.Text = "Reset Count  : " + ResetCount;
            Txt_ChallengeMode.Text = "Challenge Mode  : " + Ironman_Mode.ToString();
            Txt_Timer.Text = "Your Time  : " + Min.ToString("D2") + " : " + Sec.ToString("D2") + " : " + MiliSec.ToString("D2");
            if(Ironman_Mode==true)
            {
                Txt_TimeToBeat.Text = "Push the box to the point under 2 min !";
            }
            else if(Ironman_Mode==false)
            {
                Txt_TimeToBeat.Text = "Push the box to the point !";
            }
            if(Ironman_Mode == true && Min == 2 && Sec == 0 && MiliSec == 0)
            {
                GameEnd = true;
                GameTimer.Stop();
            }

            //player move
            if (Player.Left == P_EndPosX + P_SpawnX && Player.Top == P_EndPosY + P_SpawnY)
            {
                if (P_GoLeft == true)
                {
                    P_EndPosX -= Move_Distance;
                    Txt_DebugPlayerDi.Text = "Player Go : Left"; //Debug
                    foreach (Box Box in Boxes)
                    {
                        if (Player.Bounds.IntersectsWith(Box.GetObj().Bounds) == true)
                        {
                            State = 9;
                        }
                        else
                        {
                            State = 5;
                        }
                    }
                }
                if (P_GoRight == true)
                {
                    P_EndPosX += Move_Distance;
                    Txt_DebugPlayerDi.Text = "Player Go : Right"; //Debug
                    foreach (Box Box in Boxes)
                    {
                        if (Player.Bounds.IntersectsWith(Box.GetObj().Bounds) == true)
                        {
                            State = 10;
                        }
                        else
                        {
                            State = 6;
                        }
                    }
                }
                if (P_GoUp == true)
                {
                    P_EndPosY -= Move_Distance;
                    Txt_DebugPlayerDi.Text = "Player Go : Up"; //Debug
                    foreach (Box Box in Boxes)
                    {
                        if (Player.Bounds.IntersectsWith(Box.GetObj().Bounds) == true)
                        {
                            State = 11;
                        }
                        else
                        {
                            State = 7;
                        }
                    }

                }
                if (P_GoDown == true)
                {
                    P_EndPosY += Move_Distance;
                    Txt_DebugPlayerDi.Text = "Player Go : Down"; //Debug
                    foreach (Box Box in Boxes)
                    {
                        if (Player.Bounds.IntersectsWith(Box.GetObj().Bounds) == true)
                        {
                            State = 8;
                        }
                        else
                        {
                            State = 4;
                        }
                    }
                }
            }
            //P_EndPos+P_Spawn will be where the sprite will ended up
            if (Player.Left < P_EndPosX + P_SpawnX)
            {
                Player.Left += CharMove_Speed;
            }
            if (Player.Left > P_EndPosX + P_SpawnX)
            {
                Player.Left -= CharMove_Speed;
            }
            if (Player.Top < P_EndPosY + P_SpawnY)
            {
                Player.Top += CharMove_Speed;
            }
            if (Player.Top > P_EndPosY + P_SpawnY)
            {
                Player.Top -= CharMove_Speed;
            }
            //Boxes Colision with Player or Walls
           foreach(Box Box in Boxes)
            {
                if (Box.GetObj() is PictureBox && ((string)Box.GetObj().Tag == "BoxS1" || (string)Box.GetObj().Tag == "BoxS2" || (string)Box.GetObj().Tag == "BoxS3"))
                {
                    if (Player.Bounds.IntersectsWith(Box.GetObj().Bounds))
                    {
                        if (Player.Left < P_EndPosX + P_SpawnX)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                for (int j = 0; j < Boxes.Count; j++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Boxes[j].GetObj().Bounds) && Boxes[j] != Box)
                                    {
                                        P_EndPosX -= Move_Distance/2;
                                        Box.Move(-(CharMove_Speed * 2), 0);
                                        break;
                                    }
                                }
                                for (int k = 0; k < Walls.Count; k++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Walls[k].Bounds))
                                    {
                                        P_EndPosX -= Move_Distance/2;
                                        Box.Move(-(CharMove_Speed * 2), 0);
                                        break;
                                    }
                                }
                                Box.Move(CharMove_Speed, 0);
                            }
                        }
                        else if (Player.Left > P_EndPosX + P_SpawnX)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                for (int j = 0; j < Boxes.Count; j++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Boxes[j].GetObj().Bounds) && Boxes[j] != Box)
                                    {
                                        P_EndPosX += Move_Distance/2;
                                        Box.Move(CharMove_Speed*2, 0);
                                        break;
                                    }
                                }
                                for (int k = 0; k < Walls.Count; k++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Walls[k].Bounds))
                                    {
                                        P_EndPosX += Move_Distance/2;
                                        Box.Move(CharMove_Speed * 2, 0);
                                        break;
                                    }
                                }
                                Box.Move(-CharMove_Speed, 0);
                            }
                        }
                        else if (Player.Top < P_EndPosY + P_SpawnY)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                for (int j = 0; j < Boxes.Count; j++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Boxes[j].GetObj().Bounds) && Boxes[j] != Box)
                                    {
                                        P_EndPosY -= Move_Distance/2;
                                        Box.Move(0, -(CharMove_Speed * 2));
                                        break;
                                    }
                                }
                                for (int k = 0; k < Walls.Count; k++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Walls[k].Bounds))
                                    {
                                        P_EndPosY -= Move_Distance/2;
                                        Box.Move(0, -(CharMove_Speed * 2));
                                        break;
                                    }
                                }
                                Box.Move(0, CharMove_Speed);
                            }
                        }
                        else if (Player.Top > P_EndPosY + P_SpawnY)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                for (int j = 0; j < Boxes.Count; j++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Boxes[j].GetObj().Bounds) && Boxes[j] != Box)
                                    {
                                        P_EndPosY += Move_Distance/2;
                                        Box.Move(0, CharMove_Speed*2);
                                        break;
                                    }
                                }
                                for (int k = 0; k < Walls.Count; k++)
                                {
                                    if (Box.GetObj().Bounds.IntersectsWith(Walls[k].Bounds))
                                    {
                                        P_EndPosY += Move_Distance/2;
                                        Box.Move(0, CharMove_Speed * 2);
                                        break;
                                    }
                                }
                                Box.Move(0, -CharMove_Speed);
                                
                            }
                        }
                    }
                }
            }
            //Player colision with wall
            for (int i = 0; i < Walls.Count; i++)
            {
                if (Player.Bounds.IntersectsWith(Walls[i].Bounds)&&Walls[i].Visible)
                {
                    if (Player.Left < P_EndPosX + P_SpawnX)
                    {
                        P_EndPosX -= Move_Distance;
                    }
                    else if (Player.Left > P_EndPosX + P_SpawnX)
                    {
                        P_EndPosX += Move_Distance;
                    }
                    else if (Player.Top < P_EndPosY + P_SpawnY)
                    {
                        P_EndPosY -= Move_Distance;
                    }
                    else if (Player.Top > P_EndPosY + P_SpawnY)
                    {
                        P_EndPosY += Move_Distance;
                    }
                }
            }
            //Player colision with unmovable box
            for (int i = 0; i < Boxes.Count; i++)
            {
                if (Player.Bounds.IntersectsWith(Boxes[i].GetObj().Bounds)&& Boxes[i].Movable == false)
                {
                    if (Player.Left < P_EndPosX + P_SpawnX)
                    {
                        P_EndPosX -= Move_Distance;
                    }
                    else if (Player.Left > P_EndPosX + P_SpawnX)
                    {
                        P_EndPosX += Move_Distance;
                    }
                    else if (Player.Top < P_EndPosY + P_SpawnY)
                    {
                        P_EndPosY -= Move_Distance;
                    }
                    else if (Player.Top > P_EndPosY + P_SpawnY)
                    {
                        P_EndPosY += Move_Distance;
                    }
                }
            }

            //Box Collision with Drop Off
            foreach (DropOff DF in DropOffs) 
            {
                foreach (Box Box in Boxes)
                {
                    if (DF.GetStage() == 1 && Box.GetStage() == 1)
                    {
                        if (Box.GetObj().Bounds.IntersectsWith(DF.GetObj().Bounds)&&DF.GetOccupied() == false)
                        {
                            DF.SetOccupied(true);
                            Room1_Count++;
                        }
                        else if(Player.Bounds.IntersectsWith(DF.GetObj().Bounds) && DF.GetOccupied() == true)
                        {
                            DF.SetOccupied(false);
                            Room1_Count--;
                        }
                    }
                    else if (DF.GetStage() == 2 && Box.GetStage() == 2)
                    {
                        if (Box.GetObj().Bounds.IntersectsWith(DF.GetObj().Bounds) && DF.GetOccupied() == false)
                        {
                            DF.SetOccupied(true);
                            Room2_Count++;
                        }
                        else if (Player.Bounds.IntersectsWith(DF.GetObj().Bounds) && DF.GetOccupied() == true)
                        {
                            DF.SetOccupied(false);
                            Room2_Count--;
                        }
                    }
                    else if (DF.GetStage() == 3 && Box.GetStage() == 3)
                    {
                        if (Box.GetObj().Bounds.IntersectsWith(DF.GetObj().Bounds) && DF.GetOccupied() == false)
                        {
                            DF.SetOccupied(true);
                            Room3_Count++;
                        }
                        else if (Player.Bounds.IntersectsWith(DF.GetObj().Bounds) && DF.GetOccupied() == true)
                        {
                            DF.SetOccupied(false);
                            Room3_Count--;
                        }
                    }
                }
            }
            //Checkpoint collision
            foreach (Checkpoint CP in Checkpoints)
            {
                if(Player.Bounds.IntersectsWith(CP.GetObj().Bounds))
                {
                    if(CP.GetStage() == 1 && checkpoint <= 1 && Ironman_Mode == false)
                    {
                        checkpoint = 1;
                    }
                    else if (CP.GetStage() == 2 && checkpoint <= 2 && Ironman_Mode == false)
                    {
                        checkpoint = 2;
                    }
                    else if (CP.GetStage() == 3 && checkpoint <= 3 && Ironman_Mode == false)
                    {
                        checkpoint = 3;
                    }
                }
            }
            //spawning covid
            if (counter % 30 == 0)
            {
                foreach (CovBox CB in CovBoxes)
                {
                    Covids.Add(CB.CreateCov("Down"));
                    Controls.Add(Covids[Covids.Count-1].GetObj());
                    Covids[Covids.Count - 1].GetObj().BringToFront();
                }
            }
            //covid move
            foreach(Covid Covid in Covids)
            {
                Covid.Move(CharMove_Speed);
            }
            //covid hit something
            foreach(Covid Covid in Covids.ToList())
            {
                foreach(Control Obj in this.Controls)
                {
                    if(Covid.GetObj().Bounds.IntersectsWith(Obj.Bounds))
                    {
                        if(Obj.Tag == "Player"&&PlayerIsHit==false)
                        {
                            Life = Life - 1;
                            PlayerIsHit = true;
                        }
                        if(Obj.Tag == "Wall" || Obj.Tag == "BoxS1" || Obj.Tag == "BoxS2" || Obj.Tag == "BoxS3" || Obj.Tag == "Player")
                        {
                            Controls.Remove(Covid.GetObj());
                            Covids.Remove(Covid);
                        }    
                        
                    }
                }
            }
            //Spike FlipFlop
            if(counter % 15 == 0)
            {
                foreach(Spike Spike in Spikes)
                {
                    Spike.FlipFlopActivate();
                }
            }
            //Player Hit Spike
            foreach(Spike Spike in Spikes)
            {
                if(Player.Bounds.IntersectsWith(Spike.GetObj().Bounds))
                {
                    if(Spike.GetActivated()==true && PlayerIsHit == false)
                    {
                        Life = Life - 1;
                        PlayerIsHit = true;
                    }
                }
            }
            if(PlayerIsHit==true)
            {
                if (counter % 2 == 0 )
                {
                    FlickerPlayer();
                    FlickerCount++;
                }
                else if(FlickerCount > 11)
                {
                    PlayerIsHit = false;
                    FlickerCount = 0;
                }
            }
            //Room Code
            //Room 1
            if (Room1_Count>=Room1_Max)
            {
                Door1.Visible = false;
                for(int i=0;i<Room1_Max;i++)
                {
                    foreach(Box Box in Boxes)
                    {
                        if(Box.GetStage()==1)
                        {
                            Box.Movable=false;
                        }
                    }
                }
            }
            else
            {
                Door1.Visible = true;
            }
            //Room 2
            if (Room2_Count >= Room2_Max)
            {
                Door2.Visible = false;
                for (int i = 0; i < Room2_Max; i++)
                {
                    foreach (Box Box in Boxes)
                    {
                        if (Box.GetStage() == 2)
                        {
                            Box.Movable = false;
                        }
                    }
                }
            }
            else
            {
                Door2.Visible = true;
            }
            //Room 3
            if (Room3_Count >= Room3_Max)
            {
                //Door3.Visible = false;
                for (int i = 0; i < Room3_Max; i++)
                {
                    foreach (Box Box in Boxes)
                    {
                        if (Box.GetStage() == 3)
                        {
                            Box.Movable = false;
                        }
                    }
                }
                GameEnd = true;
                Txt_Timer.Text = "You beat the game in  : " + Min.ToString("D2") + " : " + Sec.ToString("D2") + " : " + MiliSec.ToString("D2");
                GameTimer.Stop();
            }
            UpdateAnim();
            UpdateTimer();
        }
        
        //Put Object into List
        private void UpdateLists()
        {
            UpdateBoxList();
            UpdateWallList();
            UpdateCheckpointList();
            UpdateDropOffList();
            UpdateCovBoxList();
            UpdateSpikeList();
        }
        private void UpdateBoxList()
        {
            foreach(Control Box in this.Controls)
            {
                if(Box is PictureBox && ((string)Box.Tag == "BoxS1"|| (string)Box.Tag == "BoxS2"|| (string)Box.Tag == "BoxS3"))
                {
                    if((string)Box.Tag == "BoxS1")
                    {
                        Boxes.Add(new Box(Box, 1));
                        BoxNameList += Box.Name + " Stage 1 , ";
                        Room1_Max++;
                    }
                    else if ((string)Box.Tag == "BoxS2")
                    {
                        Boxes.Add(new Box(Box, 2));
                        BoxNameList += Box.Name + " Stage 2 , ";
                        Room2_Max++;
                    }
                    else if ((string)Box.Tag == "BoxS3")
                    {
                        Boxes.Add(new Box(Box, 3));
                        BoxNameList += Box.Name + " Stage 3 , ";
                        Room3_Max++;
                    }
                }
            }
        }

        private void UpdateWallList()
        {
            foreach (Control Wall in this.Controls)
            {
                if (Wall is PictureBox && ((string)Wall.Tag == "Wall" || ((string)Wall.Tag == "CovBox" )))
                {
                    Walls.Add(Wall);
                    WallNameList += Wall.Name + " , ";
                }
            }
        }

        private void UpdateCheckpointList()
        {
            foreach (Control CP in this.Controls)
            {
                if (CP is PictureBox && ((string)CP.Tag == "CP1" || (string)CP.Tag == "CP2" || (string)CP.Tag == "CP3"))
                {
                    if ((string)CP.Tag == "CP1")
                    {
                        Checkpoints.Add(new Checkpoint(CP, 1));
                    }
                    else if ((string)CP.Tag == "CP2")
                    {
                        Checkpoints.Add(new Checkpoint(CP, 2));
                    }
                    else if ((string)CP.Tag == "CP3")
                    {
                        Checkpoints.Add(new Checkpoint(CP, 3));
                    }
                }
            }
        }

        private void UpdateDropOffList()
        {
            foreach (Control DF in this.Controls)
            {
                if (DF is PictureBox && ((string)DF.Tag == "DF1" || (string)DF.Tag == "DF2" || (string)DF.Tag == "DF3"))
                {
                    if ((string)DF.Tag == "DF1")
                    {
                        DropOffs.Add(new DropOff(DF, 1));
                    }
                    else if ((string)DF.Tag == "DF2")
                    {
                        DropOffs.Add(new DropOff(DF, 2));
                    }
                    else if ((string)DF.Tag == "DF3")
                    {
                        DropOffs.Add(new DropOff(DF, 3));
                    }
                }
            }
        }
        public void UpdateCovBoxList()
        {
            foreach(Control CB in this.Controls)
            {
                if(CB is PictureBox && (string)CB.Tag == "CovBox")
                {
                    CovBoxes.Add(new CovBox(CB));
                }
            }
        }
        public void UpdateSpikeList()
        {
            foreach(Control SP in this.Controls)
            {
                if(SP is PictureBox && (string)SP.Tag == "Spike")
                {
                    Spikes.Add(new Spike(SP, false));
                }
            }
        }
        //Restart Game
        private void GameRestart()
        {
            if(checkpoint==1)
            {
                P_EndPosX = 0;
                P_EndPosY = 0;
                P_SpawnX = Checkpoint1.Left;
                P_SpawnY = Checkpoint1.Top;
                Player.Left = Checkpoint1.Left;
                Player.Top = Checkpoint1.Top;
                ResetCount++;
            }
            if (checkpoint == 2)
            {
                P_EndPosX = 0;
                P_EndPosY = 0;
                P_SpawnX = Checkpoint2.Left;
                P_SpawnY = Checkpoint2.Top;
                Player.Left = Checkpoint2.Left;
                Player.Top = Checkpoint2.Top;
                ResetCount++;
            }
            if (checkpoint == 3)
            {
                P_EndPosX = 0;
                P_EndPosY = 0;
                P_SpawnX = Checkpoint3.Left;
                P_SpawnY = Checkpoint3.Top;
                Player.Left = Checkpoint3.Left;
                Player.Top = Checkpoint3.Top;
                ResetCount++;
            }
            if(Ironman_Mode == true)
            {
                foreach (Box Box in Boxes)
                {
                    Box.SetMovable(true);
                }
                Ironman_Mode = true;
                ResetTimer();
            }
            if (Dead == true)
            {
                Life = 3;
                Dead = false;
                GameTimer.Start();
            }
            if (GameEnd == true)
            {
                P_EndPosX = 0;
                P_EndPosY = 0;
                P_SpawnX = Checkpoint1.Left;
                P_SpawnY = Checkpoint1.Top;
                Player.Left = Checkpoint1.Left;
                Player.Top = Checkpoint1.Top;
                Room1_Count = 0;
                Room2_Count = 0;
                Room3_Count = 0;
                ResetCount = 0;
                foreach (Box Box in Boxes)
                {
                    Box.SetMovable(true);
                }
                GameTimer.Start();
            }
            PlayerStatsReset();
            ResetBoxPos();
            ResetDF();
            if(Ironman_Mode==true || GameEnd == true)
            {
                ResetTimer();
                if(GameEnd==true)
                {
                    GameEnd = false;
                    checkpoint = 0;
                }
            }
        }

        private void ResetBoxPos()
        {
            foreach (Box Box in Boxes)
            {
                if(Box.Movable == true)
                {
                    Box.ResetPos();
                }
            }
        }
        private void ResetDF()
        {
            foreach(DropOff DF in DropOffs)
            {
                DF.SetOccupied(false);
                if(checkpoint == 1)
                {
                    Room1_Count = 0;
                    Room2_Count = 0;
                    Room3_Count = 0;
                }
                else if (checkpoint == 2)
                {
                    Room2_Count = 0;
                    Room3_Count = 0;
                }
                else if (checkpoint == 3)
                {
                    Room3_Count = 0;
                }
            }
        }
        private void FlickerPlayer()
        {
            if (Player.Visible == true)
            {
                Player.Visible = false;
            }
            else if (Player.Visible == false)
            {
                Player.Visible = true;
            }
        }
        private void PlayerStatsReset()
        {
            FlickerCount = 0;
            PlayerIsHit = false;
            Player.Visible = true;
            if(Ironman_Mode==true)
            {
                Life = 1;
            }
            else if (Ironman_Mode == false)
            {
                Life = 3;
            }
            State = 0;
        }
        //Animaton stuff
        private void LoadSprite()
        {
            // 0 = idle front 1 = idle left 2 = idle right 3 = idle back 
            // 4 = walk down 5 = walk left 6 = walk right 7 = walk up 
            // 8 = push down 9 = push left 10 = push right 11 = push up
            P_Anim[0] = new Bitmap[4];
            P_Anim[1] = new Bitmap[4];
            P_Anim[2] = new Bitmap[4];
            P_Anim[3] = new Bitmap[4];
            P_Anim[4] = new Bitmap[4];
            P_Anim[5] = new Bitmap[2];
            P_Anim[6] = new Bitmap[2];
            P_Anim[7] = new Bitmap[4];
            P_Anim[8] = new Bitmap[4];
            P_Anim[9] = new Bitmap[4];
            P_Anim[10] = new Bitmap[4];
            P_Anim[11] = new Bitmap[4];
            for (int i=0;i<P_Anim.Length;i++)
            {
                for(int j=0;j<P_Anim[i].Length;j++)
                {
                    P_Anim[i][j] = new Bitmap("./Sprites/Player/" + i + "/" + i +"_" + j + ".png");
                }
            }
            Player.Image = P_Anim[State][FrameNum];
        }
        private void UpdateAnim()
        {
            counter = counter + 1;
            if(counter>59)
            {
                counter = 0;
            }
            if(counter%2==0)
            {
                FrameNum = ++FrameNum % P_Anim[State].Length;
                Player.Image = P_Anim[State][FrameNum];
            }
            foreach(Covid Covid in Covids)
            {
                Covid.UpdateSprite(counter);
            }
            foreach(CovBox CB in CovBoxes)
            {
                CB.UpdateSprite(counter);
            }
        }
        private void UpdateTimer()
        {
            MiliSec = MiliSec + 1;
            if(MiliSec == 10)
            {
                MiliSec = 0;
                Sec = Sec + 1;
            }
            if (Sec == 60)
            {
                Sec = 0;
                Min = Min + 1;
            }
        }
        private void ResetTimer()
        {
            MiliSec = 0;
            Sec = 0;
            Min = 0;
        }
    }
}
