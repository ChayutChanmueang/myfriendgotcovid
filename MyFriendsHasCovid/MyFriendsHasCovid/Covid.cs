﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class Covid : Game_Object
    {
        string Dirc;
        private Bitmap[] Cov_Anim = new Bitmap[4];
        int FrameNum = 0;
        public Covid(Control Obj,String Dirc) : base(Obj)
        {
            this.Dirc = Dirc;
            Movable = true;
            LoadSprite();
            Obj.BackColor = Color.Transparent;
        }
        public void Move(int Speed)
        {
            if (Dirc == "Left")
            {
                Obj.Left -= Speed;
            }
            else if (Dirc == "Right")
            {
                Obj.Left += Speed;
            }
            else if (Dirc == "Up")
            {
                Obj.Top -= Speed;
            }
            else if (Dirc == "Down")
            {
                Obj.Top += Speed;
            }
        }
        private void LoadSprite()
        {
            for(int i=0;i<Cov_Anim.Length;i++)
            {
                Cov_Anim[i] = new Bitmap("./Sprites/Covid/Bullet" + i + ".png");
            }
            Obj.BackgroundImage = Cov_Anim[0];
        }
        public void UpdateSprite(int counter)
        {
            if(counter%2==0)
            {
                FrameNum = ++FrameNum % Cov_Anim.Length;
                Obj.BackgroundImage = Cov_Anim[FrameNum];

            }
        }
    }
}
