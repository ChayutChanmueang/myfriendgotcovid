﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    class CovBox : Game_Object
    {
        private Bitmap[] BoxCov_Anim = new Bitmap[2];
        int FrameNum = 0;
        public CovBox(Control Obj) : base(Obj)
        {
            LoadSprite();
        }
        public Covid CreateCov(string Dirc)
        {
            PictureBox Cov = new PictureBox();
            Cov.Height = 48;
            Cov.Width = 48;
            Cov.Tag = "Covid";
            Cov.BackColor = Color.Transparent;
            Covid CovObj = new Covid(Cov, Dirc);
            Cov.Top = Obj.Top;
            Cov.Left = Obj.Left;
            return CovObj;
        }
        private void LoadSprite()
        {
            for (int i = 0; i < BoxCov_Anim.Length; i++)
            {
                BoxCov_Anim[i] = new Bitmap("./Sprites/CovBox/CardboardBox" + i + ".png");
            }
            Obj.BackgroundImage = BoxCov_Anim[0];
        }
        public void UpdateSprite(int counter)
        {
            if (counter % 2 == 0)
            {
                FrameNum = ++FrameNum % BoxCov_Anim.Length;
                Obj.BackgroundImage = BoxCov_Anim[FrameNum];

            }
        }
    }
}
