﻿namespace MyFriendsHasCovid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.Txt_PosDebug = new System.Windows.Forms.Label();
            this.Txt_PosDebug2 = new System.Windows.Forms.Label();
            this.Txt_SizeDebug = new System.Windows.Forms.Label();
            this.Txt_DebugPlayerDi = new System.Windows.Forms.Label();
            this.Txt_BoxNameDebug = new System.Windows.Forms.Label();
            this.Txt_WallNameDebug = new System.Windows.Forms.Label();
            this.Txt_RoomCountDebug = new System.Windows.Forms.Label();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.Door2 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.Box19 = new System.Windows.Forms.PictureBox();
            this.Box18 = new System.Windows.Forms.PictureBox();
            this.Box17 = new System.Windows.Forms.PictureBox();
            this.Box16 = new System.Windows.Forms.PictureBox();
            this.Box15 = new System.Windows.Forms.PictureBox();
            this.Door1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Box04 = new System.Windows.Forms.PictureBox();
            this.BadBox1 = new System.Windows.Forms.PictureBox();
            this.Box03 = new System.Windows.Forms.PictureBox();
            this.Box02 = new System.Windows.Forms.PictureBox();
            this.Wall1 = new System.Windows.Forms.PictureBox();
            this.Box01 = new System.Windows.Forms.PictureBox();
            this.Player = new System.Windows.Forms.PictureBox();
            this.Checkpoint1 = new System.Windows.Forms.PictureBox();
            this.Checkpoint3 = new System.Windows.Forms.PictureBox();
            this.Checkpoint2 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.pictureBox64 = new System.Windows.Forms.PictureBox();
            this.pictureBox65 = new System.Windows.Forms.PictureBox();
            this.pictureBox66 = new System.Windows.Forms.PictureBox();
            this.pictureBox67 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Lifes = new System.Windows.Forms.PictureBox();
            this.Txt_ResetCount = new System.Windows.Forms.Label();
            this.Txt_ChallengeMode = new System.Windows.Forms.Label();
            this.Txt_Timer = new System.Windows.Forms.Label();
            this.Txt_TimeToBeat = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Door2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Door1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BadBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Wall1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lifes)).BeginInit();
            this.SuspendLayout();
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 60;
            this.GameTimer.Tick += new System.EventHandler(this.MainGameTimerEvent);
            // 
            // Txt_PosDebug
            // 
            this.Txt_PosDebug.AutoSize = true;
            this.Txt_PosDebug.Location = new System.Drawing.Point(10, 14);
            this.Txt_PosDebug.Name = "Txt_PosDebug";
            this.Txt_PosDebug.Size = new System.Drawing.Size(88, 17);
            this.Txt_PosDebug.TabIndex = 2;
            this.Txt_PosDebug.Text = "Player.Left : ";
            // 
            // Txt_PosDebug2
            // 
            this.Txt_PosDebug2.AutoSize = true;
            this.Txt_PosDebug2.Location = new System.Drawing.Point(10, 31);
            this.Txt_PosDebug2.Name = "Txt_PosDebug2";
            this.Txt_PosDebug2.Size = new System.Drawing.Size(95, 17);
            this.Txt_PosDebug2.TabIndex = 3;
            this.Txt_PosDebug2.Text = "P_EndPosX : ";
            // 
            // Txt_SizeDebug
            // 
            this.Txt_SizeDebug.AutoSize = true;
            this.Txt_SizeDebug.Location = new System.Drawing.Point(10, 48);
            this.Txt_SizeDebug.Name = "Txt_SizeDebug";
            this.Txt_SizeDebug.Size = new System.Drawing.Size(73, 17);
            this.Txt_SizeDebug.TabIndex = 8;
            this.Txt_SizeDebug.Text = "Player.X : ";
            // 
            // Txt_DebugPlayerDi
            // 
            this.Txt_DebugPlayerDi.AutoSize = true;
            this.Txt_DebugPlayerDi.Location = new System.Drawing.Point(10, 65);
            this.Txt_DebugPlayerDi.Name = "Txt_DebugPlayerDi";
            this.Txt_DebugPlayerDi.Size = new System.Drawing.Size(83, 17);
            this.Txt_DebugPlayerDi.TabIndex = 10;
            this.Txt_DebugPlayerDi.Text = "Player Go : ";
            // 
            // Txt_BoxNameDebug
            // 
            this.Txt_BoxNameDebug.AutoSize = true;
            this.Txt_BoxNameDebug.Location = new System.Drawing.Point(342, 9);
            this.Txt_BoxNameDebug.Name = "Txt_BoxNameDebug";
            this.Txt_BoxNameDebug.Size = new System.Drawing.Size(104, 17);
            this.Txt_BoxNameDebug.TabIndex = 14;
            this.Txt_BoxNameDebug.Text = "Boxes names : ";
            // 
            // Txt_WallNameDebug
            // 
            this.Txt_WallNameDebug.AutoSize = true;
            this.Txt_WallNameDebug.Location = new System.Drawing.Point(342, 31);
            this.Txt_WallNameDebug.Name = "Txt_WallNameDebug";
            this.Txt_WallNameDebug.Size = new System.Drawing.Size(93, 17);
            this.Txt_WallNameDebug.TabIndex = 18;
            this.Txt_WallNameDebug.Text = "Wall names : ";
            // 
            // Txt_RoomCountDebug
            // 
            this.Txt_RoomCountDebug.AutoSize = true;
            this.Txt_RoomCountDebug.Location = new System.Drawing.Point(342, 48);
            this.Txt_RoomCountDebug.Name = "Txt_RoomCountDebug";
            this.Txt_RoomCountDebug.Size = new System.Drawing.Size(108, 17);
            this.Txt_RoomCountDebug.TabIndex = 28;
            this.Txt_RoomCountDebug.Text = "Room 1 count : ";
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox50.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox50.Image")));
            this.pictureBox50.Location = new System.Drawing.Point(480, 576);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(48, 48);
            this.pictureBox50.TabIndex = 78;
            this.pictureBox50.TabStop = false;
            this.pictureBox50.Tag = "BoxS3";
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox49.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox49.Image")));
            this.pictureBox49.Location = new System.Drawing.Point(576, 576);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(48, 48);
            this.pictureBox49.TabIndex = 78;
            this.pictureBox49.TabStop = false;
            this.pictureBox49.Tag = "BoxS3";
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox48.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox48.Image")));
            this.pictureBox48.Location = new System.Drawing.Point(576, 528);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(48, 48);
            this.pictureBox48.TabIndex = 77;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.Tag = "BoxS3";
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox47.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox47.Image")));
            this.pictureBox47.Location = new System.Drawing.Point(528, 528);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(48, 48);
            this.pictureBox47.TabIndex = 76;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Tag = "BoxS3";
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox46.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox46.Image")));
            this.pictureBox46.Location = new System.Drawing.Point(480, 528);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(48, 48);
            this.pictureBox46.TabIndex = 75;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Tag = "BoxS3";
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox45.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox45.Image")));
            this.pictureBox45.Location = new System.Drawing.Point(528, 480);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(48, 48);
            this.pictureBox45.TabIndex = 74;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Tag = "BoxS3";
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox44.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox44.Image")));
            this.pictureBox44.Location = new System.Drawing.Point(480, 432);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(48, 48);
            this.pictureBox44.TabIndex = 73;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Tag = "BoxS3";
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox43.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox43.Image")));
            this.pictureBox43.Location = new System.Drawing.Point(384, 528);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(48, 48);
            this.pictureBox43.TabIndex = 72;
            this.pictureBox43.TabStop = false;
            this.pictureBox43.Tag = "BoxS3";
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox34.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.ChairWood0;
            this.pictureBox34.Location = new System.Drawing.Point(432, 528);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(48, 48);
            this.pictureBox34.TabIndex = 63;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Tag = "Wall";
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.Color.Coral;
            this.pictureBox33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox33.BackgroundImage")));
            this.pictureBox33.Location = new System.Drawing.Point(336, 432);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(48, 240);
            this.pictureBox33.TabIndex = 62;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Tag = "Wall";
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.Coral;
            this.pictureBox32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox32.BackgroundImage")));
            this.pictureBox32.Location = new System.Drawing.Point(336, 672);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(384, 48);
            this.pictureBox32.TabIndex = 61;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Tag = "Wall";
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox31.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.TV0;
            this.pictureBox31.Location = new System.Drawing.Point(624, 528);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(48, 48);
            this.pictureBox31.TabIndex = 60;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.Tag = "Wall";
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.Coral;
            this.pictureBox30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox30.BackgroundImage")));
            this.pictureBox30.Location = new System.Drawing.Point(672, 480);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(48, 192);
            this.pictureBox30.TabIndex = 59;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.Tag = "Wall";
            // 
            // Door2
            // 
            this.Door2.BackColor = System.Drawing.Color.Red;
            this.Door2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Door2.BackgroundImage")));
            this.Door2.Location = new System.Drawing.Point(672, 432);
            this.Door2.Name = "Door2";
            this.Door2.Size = new System.Drawing.Size(48, 48);
            this.Door2.TabIndex = 58;
            this.Door2.TabStop = false;
            this.Door2.Tag = "Wall";
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.Coral;
            this.pictureBox29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox29.BackgroundImage")));
            this.pictureBox29.Location = new System.Drawing.Point(720, 528);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(192, 48);
            this.pictureBox29.TabIndex = 57;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.Tag = "Wall";
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.Coral;
            this.pictureBox28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox28.BackgroundImage")));
            this.pictureBox28.Location = new System.Drawing.Point(912, 480);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(96, 48);
            this.pictureBox28.TabIndex = 56;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.Tag = "Wall";
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.Coral;
            this.pictureBox27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox27.BackgroundImage")));
            this.pictureBox27.Location = new System.Drawing.Point(1008, 432);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(48, 48);
            this.pictureBox27.TabIndex = 55;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Tag = "Wall";
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox26.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox26.Image")));
            this.pictureBox26.Location = new System.Drawing.Point(1008, 288);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(48, 48);
            this.pictureBox26.TabIndex = 54;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Tag = "BoxS2";
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox25.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox25.Image")));
            this.pictureBox25.Location = new System.Drawing.Point(1008, 336);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(48, 48);
            this.pictureBox25.TabIndex = 53;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Tag = "BoxS2";
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox19.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.ChairDesk0;
            this.pictureBox19.Location = new System.Drawing.Point(864, 336);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(48, 48);
            this.pictureBox19.TabIndex = 48;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Tag = "Wall";
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.COM0;
            this.pictureBox18.Location = new System.Drawing.Point(960, 336);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(48, 48);
            this.pictureBox18.TabIndex = 47;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Tag = "Wall";
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Coral;
            this.pictureBox17.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Stairs0;
            this.pictureBox17.Location = new System.Drawing.Point(1056, 336);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(48, 96);
            this.pictureBox17.TabIndex = 46;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Tag = "Wall";
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Coral;
            this.pictureBox16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox16.BackgroundImage")));
            this.pictureBox16.Location = new System.Drawing.Point(1104, 240);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(48, 144);
            this.pictureBox16.TabIndex = 45;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Tag = "Wall";
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Coral;
            this.pictureBox15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox15.BackgroundImage")));
            this.pictureBox15.Location = new System.Drawing.Point(960, 192);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(192, 48);
            this.pictureBox15.TabIndex = 44;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Tag = "Wall";
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox14.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Speaker0;
            this.pictureBox14.Location = new System.Drawing.Point(912, 192);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(48, 96);
            this.pictureBox14.TabIndex = 43;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Tag = "Wall";
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(576, 192);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(48, 48);
            this.pictureBox20.TabIndex = 42;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Tag = "BoxS1";
            // 
            // Box19
            // 
            this.Box19.BackColor = System.Drawing.Color.Transparent;
            this.Box19.Image = ((System.Drawing.Image)(resources.GetObject("Box19.Image")));
            this.Box19.Location = new System.Drawing.Point(528, 192);
            this.Box19.Name = "Box19";
            this.Box19.Size = new System.Drawing.Size(48, 48);
            this.Box19.TabIndex = 41;
            this.Box19.TabStop = false;
            this.Box19.Tag = "BoxS1";
            // 
            // Box18
            // 
            this.Box18.BackColor = System.Drawing.Color.Transparent;
            this.Box18.Image = ((System.Drawing.Image)(resources.GetObject("Box18.Image")));
            this.Box18.Location = new System.Drawing.Point(528, 240);
            this.Box18.Name = "Box18";
            this.Box18.Size = new System.Drawing.Size(48, 48);
            this.Box18.TabIndex = 40;
            this.Box18.TabStop = false;
            this.Box18.Tag = "BoxS1";
            // 
            // Box17
            // 
            this.Box17.BackColor = System.Drawing.Color.Transparent;
            this.Box17.Image = ((System.Drawing.Image)(resources.GetObject("Box17.Image")));
            this.Box17.Location = new System.Drawing.Point(528, 288);
            this.Box17.Name = "Box17";
            this.Box17.Size = new System.Drawing.Size(48, 48);
            this.Box17.TabIndex = 39;
            this.Box17.TabStop = false;
            this.Box17.Tag = "BoxS1";
            // 
            // Box16
            // 
            this.Box16.BackColor = System.Drawing.Color.Transparent;
            this.Box16.Image = ((System.Drawing.Image)(resources.GetObject("Box16.Image")));
            this.Box16.Location = new System.Drawing.Point(576, 288);
            this.Box16.Name = "Box16";
            this.Box16.Size = new System.Drawing.Size(48, 48);
            this.Box16.TabIndex = 38;
            this.Box16.TabStop = false;
            this.Box16.Tag = "BoxS1";
            // 
            // Box15
            // 
            this.Box15.BackColor = System.Drawing.Color.Transparent;
            this.Box15.Image = ((System.Drawing.Image)(resources.GetObject("Box15.Image")));
            this.Box15.Location = new System.Drawing.Point(624, 288);
            this.Box15.Name = "Box15";
            this.Box15.Size = new System.Drawing.Size(48, 48);
            this.Box15.TabIndex = 37;
            this.Box15.TabStop = false;
            this.Box15.Tag = "BoxS1";
            // 
            // Door1
            // 
            this.Door1.BackColor = System.Drawing.Color.Red;
            this.Door1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Door1.BackgroundImage")));
            this.Door1.Location = new System.Drawing.Point(720, 192);
            this.Door1.Name = "Door1";
            this.Door1.Size = new System.Drawing.Size(48, 48);
            this.Door1.TabIndex = 26;
            this.Door1.TabStop = false;
            this.Door1.Tag = "Wall";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Coral;
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.Location = new System.Drawing.Point(720, 240);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(48, 144);
            this.pictureBox5.TabIndex = 25;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "Wall";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Coral;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.Location = new System.Drawing.Point(720, 144);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(240, 48);
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "Wall";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Coral;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(480, 95);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(288, 48);
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "Wall";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Coral;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(576, 384);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(144, 48);
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "Wall";
            // 
            // Box04
            // 
            this.Box04.BackColor = System.Drawing.Color.Transparent;
            this.Box04.Image = ((System.Drawing.Image)(resources.GetObject("Box04.Image")));
            this.Box04.Location = new System.Drawing.Point(816, 288);
            this.Box04.Name = "Box04";
            this.Box04.Size = new System.Drawing.Size(48, 48);
            this.Box04.TabIndex = 17;
            this.Box04.TabStop = false;
            this.Box04.Tag = "BoxS2";
            // 
            // BadBox1
            // 
            this.BadBox1.BackColor = System.Drawing.Color.Transparent;
            this.BadBox1.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.CardboardBox0;
            this.BadBox1.Location = new System.Drawing.Point(816, 240);
            this.BadBox1.Name = "BadBox1";
            this.BadBox1.Size = new System.Drawing.Size(48, 48);
            this.BadBox1.TabIndex = 15;
            this.BadBox1.TabStop = false;
            this.BadBox1.Tag = "CovBox";
            // 
            // Box03
            // 
            this.Box03.BackColor = System.Drawing.Color.Transparent;
            this.Box03.Image = ((System.Drawing.Image)(resources.GetObject("Box03.Image")));
            this.Box03.Location = new System.Drawing.Point(912, 336);
            this.Box03.Name = "Box03";
            this.Box03.Size = new System.Drawing.Size(48, 48);
            this.Box03.TabIndex = 13;
            this.Box03.TabStop = false;
            this.Box03.Tag = "BoxS2";
            // 
            // Box02
            // 
            this.Box02.BackColor = System.Drawing.Color.Transparent;
            this.Box02.Image = ((System.Drawing.Image)(resources.GetObject("Box02.Image")));
            this.Box02.Location = new System.Drawing.Point(624, 240);
            this.Box02.Name = "Box02";
            this.Box02.Size = new System.Drawing.Size(48, 48);
            this.Box02.TabIndex = 12;
            this.Box02.TabStop = false;
            this.Box02.Tag = "BoxS1";
            // 
            // Wall1
            // 
            this.Wall1.BackColor = System.Drawing.Color.Coral;
            this.Wall1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Wall1.BackgroundImage")));
            this.Wall1.Location = new System.Drawing.Point(432, 95);
            this.Wall1.Name = "Wall1";
            this.Wall1.Size = new System.Drawing.Size(48, 289);
            this.Wall1.TabIndex = 11;
            this.Wall1.TabStop = false;
            this.Wall1.Tag = "Wall";
            // 
            // Box01
            // 
            this.Box01.BackColor = System.Drawing.Color.Transparent;
            this.Box01.Image = ((System.Drawing.Image)(resources.GetObject("Box01.Image")));
            this.Box01.Location = new System.Drawing.Point(624, 192);
            this.Box01.Name = "Box01";
            this.Box01.Size = new System.Drawing.Size(48, 48);
            this.Box01.TabIndex = 9;
            this.Box01.TabStop = false;
            this.Box01.Tag = "BoxS1";
            // 
            // Player
            // 
            this.Player.BackColor = System.Drawing.Color.Transparent;
            this.Player.Image = global::MyFriendsHasCovid.Properties.Resources._0_0;
            this.Player.Location = new System.Drawing.Point(576, 240);
            this.Player.Name = "Player";
            this.Player.Size = new System.Drawing.Size(48, 48);
            this.Player.TabIndex = 0;
            this.Player.TabStop = false;
            this.Player.Tag = "Player";
            // 
            // Checkpoint1
            // 
            this.Checkpoint1.BackColor = System.Drawing.Color.Transparent;
            this.Checkpoint1.Location = new System.Drawing.Point(576, 240);
            this.Checkpoint1.Name = "Checkpoint1";
            this.Checkpoint1.Size = new System.Drawing.Size(48, 48);
            this.Checkpoint1.TabIndex = 20;
            this.Checkpoint1.TabStop = false;
            this.Checkpoint1.Tag = "CP1";
            // 
            // Checkpoint3
            // 
            this.Checkpoint3.BackColor = System.Drawing.Color.Transparent;
            this.Checkpoint3.Location = new System.Drawing.Point(624, 432);
            this.Checkpoint3.Name = "Checkpoint3";
            this.Checkpoint3.Size = new System.Drawing.Size(48, 48);
            this.Checkpoint3.TabIndex = 21;
            this.Checkpoint3.TabStop = false;
            this.Checkpoint3.Tag = "CP3";
            // 
            // Checkpoint2
            // 
            this.Checkpoint2.BackColor = System.Drawing.Color.Transparent;
            this.Checkpoint2.Location = new System.Drawing.Point(768, 192);
            this.Checkpoint2.Name = "Checkpoint2";
            this.Checkpoint2.Size = new System.Drawing.Size(48, 48);
            this.Checkpoint2.TabIndex = 19;
            this.Checkpoint2.TabStop = false;
            this.Checkpoint2.Tag = "CP2";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox9.Image = global::MyFriendsHasCovid.Properties.Resources.Floor2;
            this.pictureBox9.Location = new System.Drawing.Point(480, 144);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(48, 48);
            this.pictureBox9.TabIndex = 31;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "DF1";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(576, 144);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(48, 48);
            this.pictureBox8.TabIndex = 30;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "DF1";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(672, 144);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 48);
            this.pictureBox6.TabIndex = 29;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "DF1";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(672, 240);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(48, 48);
            this.pictureBox7.TabIndex = 27;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "DF1";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(672, 336);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(48, 48);
            this.pictureBox10.TabIndex = 32;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Tag = "DF1";
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(576, 336);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(48, 48);
            this.pictureBox11.TabIndex = 33;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Tag = "DF1";
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(480, 336);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(48, 48);
            this.pictureBox12.TabIndex = 34;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Tag = "DF1";
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(480, 240);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(48, 48);
            this.pictureBox13.TabIndex = 35;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Tag = "DF1";
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox24.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox24.Image")));
            this.pictureBox24.Location = new System.Drawing.Point(816, 384);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(48, 48);
            this.pictureBox24.TabIndex = 52;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Tag = "DF2";
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox23.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox23.Image")));
            this.pictureBox23.Location = new System.Drawing.Point(768, 384);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(48, 48);
            this.pictureBox23.TabIndex = 51;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Tag = "DF2";
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox22.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox22.Image")));
            this.pictureBox22.Location = new System.Drawing.Point(816, 336);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(48, 48);
            this.pictureBox22.TabIndex = 50;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Tag = "DF2";
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(768, 336);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(48, 48);
            this.pictureBox21.TabIndex = 49;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Tag = "DF2";
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox39.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox39.Image")));
            this.pictureBox39.Location = new System.Drawing.Point(432, 624);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(48, 48);
            this.pictureBox39.TabIndex = 71;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.Tag = "DF3";
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox40.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox40.Image")));
            this.pictureBox40.Location = new System.Drawing.Point(383, 624);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(48, 48);
            this.pictureBox40.TabIndex = 70;
            this.pictureBox40.TabStop = false;
            this.pictureBox40.Tag = "DF3";
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox41.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox41.Image")));
            this.pictureBox41.Location = new System.Drawing.Point(432, 576);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(48, 48);
            this.pictureBox41.TabIndex = 69;
            this.pictureBox41.TabStop = false;
            this.pictureBox41.Tag = "DF3";
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox42.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox42.Image")));
            this.pictureBox42.Location = new System.Drawing.Point(383, 576);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(48, 48);
            this.pictureBox42.TabIndex = 68;
            this.pictureBox42.TabStop = false;
            this.pictureBox42.Tag = "DF3";
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox35.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox35.Image")));
            this.pictureBox35.Location = new System.Drawing.Point(432, 480);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(48, 48);
            this.pictureBox35.TabIndex = 67;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Tag = "DF3";
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox36.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox36.Image")));
            this.pictureBox36.Location = new System.Drawing.Point(383, 480);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(48, 48);
            this.pictureBox36.TabIndex = 66;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Tag = "DF3";
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox37.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox37.Image")));
            this.pictureBox37.Location = new System.Drawing.Point(432, 432);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(48, 48);
            this.pictureBox37.TabIndex = 65;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Tag = "DF3";
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox38.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox38.Image")));
            this.pictureBox38.Location = new System.Drawing.Point(383, 432);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(48, 48);
            this.pictureBox38.TabIndex = 64;
            this.pictureBox38.TabStop = false;
            this.pictureBox38.Tag = "DF3";
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox51.Location = new System.Drawing.Point(912, 336);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(48, 48);
            this.pictureBox51.TabIndex = 80;
            this.pictureBox51.TabStop = false;
            this.pictureBox51.Tag = "Spike";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox1.Location = new System.Drawing.Point(864, 432);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.TabIndex = 79;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "Spike";
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.Black;
            this.pictureBox52.Location = new System.Drawing.Point(-4, 0);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(1909, 97);
            this.pictureBox52.TabIndex = 81;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackColor = System.Drawing.Color.Black;
            this.pictureBox53.Location = new System.Drawing.Point(-1, 95);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(436, 289);
            this.pictureBox53.TabIndex = 82;
            this.pictureBox53.TabStop = false;
            // 
            // pictureBox54
            // 
            this.pictureBox54.BackColor = System.Drawing.Color.Black;
            this.pictureBox54.Location = new System.Drawing.Point(-4, 384);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(340, 651);
            this.pictureBox54.TabIndex = 83;
            this.pictureBox54.TabStop = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.BackColor = System.Drawing.Color.Black;
            this.pictureBox55.Location = new System.Drawing.Point(336, 720);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(1569, 315);
            this.pictureBox55.TabIndex = 84;
            this.pictureBox55.TabStop = false;
            // 
            // pictureBox56
            // 
            this.pictureBox56.BackColor = System.Drawing.Color.Black;
            this.pictureBox56.Location = new System.Drawing.Point(720, 576);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(1185, 144);
            this.pictureBox56.TabIndex = 85;
            this.pictureBox56.TabStop = false;
            // 
            // pictureBox57
            // 
            this.pictureBox57.BackColor = System.Drawing.Color.Black;
            this.pictureBox57.Location = new System.Drawing.Point(1104, 95);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(801, 481);
            this.pictureBox57.TabIndex = 86;
            this.pictureBox57.TabStop = false;
            // 
            // pictureBox58
            // 
            this.pictureBox58.BackColor = System.Drawing.Color.Black;
            this.pictureBox58.Location = new System.Drawing.Point(912, 480);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(192, 96);
            this.pictureBox58.TabIndex = 87;
            this.pictureBox58.TabStop = false;
            // 
            // pictureBox59
            // 
            this.pictureBox59.BackColor = System.Drawing.Color.Black;
            this.pictureBox59.Location = new System.Drawing.Point(768, 96);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(336, 96);
            this.pictureBox59.TabIndex = 88;
            this.pictureBox59.TabStop = false;
            // 
            // pictureBox60
            // 
            this.pictureBox60.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox60.Location = new System.Drawing.Point(384, 528);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(48, 48);
            this.pictureBox60.TabIndex = 89;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.Tag = "Spike";
            // 
            // pictureBox61
            // 
            this.pictureBox61.BackColor = System.Drawing.Color.Coral;
            this.pictureBox61.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox61.BackgroundImage")));
            this.pictureBox61.Location = new System.Drawing.Point(336, 384);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(192, 48);
            this.pictureBox61.TabIndex = 90;
            this.pictureBox61.TabStop = false;
            this.pictureBox61.Tag = "Wall";
            // 
            // pictureBox62
            // 
            this.pictureBox62.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox62.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.CardboardBox0;
            this.pictureBox62.Location = new System.Drawing.Point(528, 384);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(48, 48);
            this.pictureBox62.TabIndex = 91;
            this.pictureBox62.TabStop = false;
            this.pictureBox62.Tag = "CovBox";
            // 
            // pictureBox63
            // 
            this.pictureBox63.BackColor = System.Drawing.Color.Wheat;
            this.pictureBox63.Location = new System.Drawing.Point(528, 528);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(48, 48);
            this.pictureBox63.TabIndex = 92;
            this.pictureBox63.TabStop = false;
            this.pictureBox63.Tag = "Spike";
            // 
            // pictureBox64
            // 
            this.pictureBox64.BackColor = System.Drawing.Color.Black;
            this.pictureBox64.Location = new System.Drawing.Point(1056, 432);
            this.pictureBox64.Name = "pictureBox64";
            this.pictureBox64.Size = new System.Drawing.Size(48, 48);
            this.pictureBox64.TabIndex = 93;
            this.pictureBox64.TabStop = false;
            // 
            // pictureBox65
            // 
            this.pictureBox65.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox65.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Fridge0;
            this.pictureBox65.Location = new System.Drawing.Point(960, 432);
            this.pictureBox65.Name = "pictureBox65";
            this.pictureBox65.Size = new System.Drawing.Size(48, 48);
            this.pictureBox65.TabIndex = 94;
            this.pictureBox65.TabStop = false;
            this.pictureBox65.Tag = "Wall";
            // 
            // pictureBox66
            // 
            this.pictureBox66.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox66.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Washer0;
            this.pictureBox66.Location = new System.Drawing.Point(864, 480);
            this.pictureBox66.Name = "pictureBox66";
            this.pictureBox66.Size = new System.Drawing.Size(48, 48);
            this.pictureBox66.TabIndex = 95;
            this.pictureBox66.TabStop = false;
            this.pictureBox66.Tag = "Wall";
            // 
            // pictureBox67
            // 
            this.pictureBox67.Location = new System.Drawing.Point(1104, 48);
            this.pictureBox67.Name = "pictureBox67";
            this.pictureBox67.Size = new System.Drawing.Size(449, 95);
            this.pictureBox67.TabIndex = 96;
            this.pictureBox67.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1111, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 24);
            this.label1.TabIndex = 97;
            this.label1.Text = "Life : ";
            // 
            // Lifes
            // 
            this.Lifes.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Health;
            this.Lifes.Location = new System.Drawing.Point(1174, 57);
            this.Lifes.Name = "Lifes";
            this.Lifes.Size = new System.Drawing.Size(75, 24);
            this.Lifes.TabIndex = 98;
            this.Lifes.TabStop = false;
            // 
            // Txt_ResetCount
            // 
            this.Txt_ResetCount.AutoSize = true;
            this.Txt_ResetCount.Font = new System.Drawing.Font("Comic Sans MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ResetCount.Location = new System.Drawing.Point(1110, 81);
            this.Txt_ResetCount.Name = "Txt_ResetCount";
            this.Txt_ResetCount.Size = new System.Drawing.Size(128, 24);
            this.Txt_ResetCount.TabIndex = 99;
            this.Txt_ResetCount.Text = "Reset Count  : ";
            // 
            // Txt_ChallengeMode
            // 
            this.Txt_ChallengeMode.AutoSize = true;
            this.Txt_ChallengeMode.Font = new System.Drawing.Font("Comic Sans MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_ChallengeMode.Location = new System.Drawing.Point(1111, 105);
            this.Txt_ChallengeMode.Name = "Txt_ChallengeMode";
            this.Txt_ChallengeMode.Size = new System.Drawing.Size(156, 24);
            this.Txt_ChallengeMode.TabIndex = 100;
            this.Txt_ChallengeMode.Text = "Challenge Mode  : ";
            // 
            // Txt_Timer
            // 
            this.Txt_Timer.AutoSize = true;
            this.Txt_Timer.Font = new System.Drawing.Font("Comic Sans MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Timer.Location = new System.Drawing.Point(1273, 81);
            this.Txt_Timer.Name = "Txt_Timer";
            this.Txt_Timer.Size = new System.Drawing.Size(112, 24);
            this.Txt_Timer.TabIndex = 101;
            this.Txt_Timer.Text = "Your Time  : ";
            // 
            // Txt_TimeToBeat
            // 
            this.Txt_TimeToBeat.AutoSize = true;
            this.Txt_TimeToBeat.Font = new System.Drawing.Font("Comic Sans MS", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_TimeToBeat.Location = new System.Drawing.Point(1273, 57);
            this.Txt_TimeToBeat.Name = "Txt_TimeToBeat";
            this.Txt_TimeToBeat.Size = new System.Drawing.Size(134, 24);
            this.Txt_TimeToBeat.TabIndex = 102;
            this.Txt_TimeToBeat.Text = "Time to beat  : ";
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::MyFriendsHasCovid.Properties.Resources.Floor;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.Txt_TimeToBeat);
            this.Controls.Add(this.Txt_Timer);
            this.Controls.Add(this.Txt_ChallengeMode);
            this.Controls.Add(this.Txt_ResetCount);
            this.Controls.Add(this.Lifes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox67);
            this.Controls.Add(this.pictureBox66);
            this.Controls.Add(this.pictureBox65);
            this.Controls.Add(this.pictureBox64);
            this.Controls.Add(this.pictureBox62);
            this.Controls.Add(this.pictureBox61);
            this.Controls.Add(this.pictureBox56);
            this.Controls.Add(this.pictureBox50);
            this.Controls.Add(this.pictureBox49);
            this.Controls.Add(this.pictureBox48);
            this.Controls.Add(this.pictureBox47);
            this.Controls.Add(this.pictureBox46);
            this.Controls.Add(this.pictureBox45);
            this.Controls.Add(this.pictureBox44);
            this.Controls.Add(this.pictureBox43);
            this.Controls.Add(this.pictureBox34);
            this.Controls.Add(this.pictureBox33);
            this.Controls.Add(this.pictureBox32);
            this.Controls.Add(this.pictureBox31);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.Door2);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.Box19);
            this.Controls.Add(this.Box18);
            this.Controls.Add(this.Box17);
            this.Controls.Add(this.Box16);
            this.Controls.Add(this.Box15);
            this.Controls.Add(this.Txt_RoomCountDebug);
            this.Controls.Add(this.Door1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.Txt_WallNameDebug);
            this.Controls.Add(this.Box04);
            this.Controls.Add(this.BadBox1);
            this.Controls.Add(this.Txt_BoxNameDebug);
            this.Controls.Add(this.Box03);
            this.Controls.Add(this.Box02);
            this.Controls.Add(this.Wall1);
            this.Controls.Add(this.Txt_DebugPlayerDi);
            this.Controls.Add(this.Box01);
            this.Controls.Add(this.Txt_SizeDebug);
            this.Controls.Add(this.Txt_PosDebug2);
            this.Controls.Add(this.Txt_PosDebug);
            this.Controls.Add(this.Player);
            this.Controls.Add(this.Checkpoint1);
            this.Controls.Add(this.Checkpoint3);
            this.Controls.Add(this.Checkpoint2);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox39);
            this.Controls.Add(this.pictureBox40);
            this.Controls.Add(this.pictureBox41);
            this.Controls.Add(this.pictureBox42);
            this.Controls.Add(this.pictureBox35);
            this.Controls.Add(this.pictureBox36);
            this.Controls.Add(this.pictureBox37);
            this.Controls.Add(this.pictureBox38);
            this.Controls.Add(this.pictureBox51);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox52);
            this.Controls.Add(this.pictureBox53);
            this.Controls.Add(this.pictureBox54);
            this.Controls.Add(this.pictureBox55);
            this.Controls.Add(this.pictureBox58);
            this.Controls.Add(this.pictureBox57);
            this.Controls.Add(this.pictureBox59);
            this.Controls.Add(this.pictureBox60);
            this.Controls.Add(this.pictureBox63);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyIsDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyIsUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Door2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Door1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BadBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Wall1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Checkpoint2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lifes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Player;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.Label Txt_PosDebug;
        private System.Windows.Forms.Label Txt_PosDebug2;
        private System.Windows.Forms.Label Txt_SizeDebug;
        private System.Windows.Forms.Label Txt_DebugPlayerDi;
        private System.Windows.Forms.PictureBox Wall1;
        private System.Windows.Forms.PictureBox Box02;
        private System.Windows.Forms.PictureBox Box03;
        private System.Windows.Forms.Label Txt_BoxNameDebug;
        private System.Windows.Forms.PictureBox BadBox1;
        private System.Windows.Forms.PictureBox Box04;
        private System.Windows.Forms.Label Txt_WallNameDebug;
        private System.Windows.Forms.PictureBox Checkpoint2;
        private System.Windows.Forms.PictureBox Checkpoint1;
        private System.Windows.Forms.PictureBox Checkpoint3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox Door1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label Txt_RoomCountDebug;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox Box16;
        private System.Windows.Forms.PictureBox Box18;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox Box19;
        private System.Windows.Forms.PictureBox Box01;
        private System.Windows.Forms.PictureBox Box15;
        private System.Windows.Forms.PictureBox Box17;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox Door2;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.PictureBox pictureBox63;
        private System.Windows.Forms.PictureBox pictureBox64;
        private System.Windows.Forms.PictureBox pictureBox65;
        private System.Windows.Forms.PictureBox pictureBox66;
        private System.Windows.Forms.PictureBox pictureBox67;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Lifes;
        private System.Windows.Forms.Label Txt_ResetCount;
        private System.Windows.Forms.Label Txt_ChallengeMode;
        private System.Windows.Forms.Label Txt_Timer;
        private System.Windows.Forms.Label Txt_TimeToBeat;
    }
}

