﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFriendsHasCovid
{
    abstract class Game_Object //: Form1
    {
        protected Control Obj;
        int[] Origin_Pos = new int[2]; //Origin_Pos[0] = Obj X Origin_Pos[1] = Obj Y
        //int[] MoveTo_Pos = new int[2]; //MoveTo_Pos[0] = Obj X MoveTo_Pos[1] = Obj Y
        public bool Movable;
        public Game_Object(Control Obj)
        {
            this.Obj = Obj;
            Origin_Pos[0] = Obj.Left;
            Origin_Pos[1] = Obj.Top;
            Movable = false;
        }
        public void Move(int X,int Y)
        {
            if(Movable==true)
            {
                Obj.Left += X;
                Obj.Top += Y;
            }
        }
        public void ResetPos()
        {
            Obj.Left = Origin_Pos[0];
            Obj.Top = Origin_Pos[1];
        }
        public Control GetObj()
        {
            return Obj;
        }
        public int X()
        {
            return Obj.Left;
        }
        public int Y()
        {
            return Obj.Top;
        }
        public void SetMovable(bool Movable)
        {
            this.Movable = Movable;
        }
        public bool GetMovable()
        {
            return Movable;
        }
        public void SetOriPos(int X,int Y)
        {
            Origin_Pos[0] = X;
            Origin_Pos[1] = Y;
        }
        public void SetOriPos()
        {
            Origin_Pos[0] = Obj.Left;
            Origin_Pos[1] = Obj.Top;
        }
    }
}
